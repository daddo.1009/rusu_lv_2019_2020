# -*- coding: utf-8 -*-
"""
Created on Thu Dec  3 09:11:47 2020

@author: student
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

mtcars = pd.read_csv('../resources/mtcars.csv')


#mtcars1 = mtcars.groupby('cyl').mean()
#print(mtcars1)
#plt.bar([4, 6, 8], mtcars1['mpg'], align='center', alpha= 0.5)
#plt.show()



#mtcars2 = mtcars[['cyl', 'wt']]
#mtcars2.boxplot(by='cyl')
#plt.show()



#mtcars3 = mtcars.groupby('am').mean()
#plt.bar([0, 1], mtcars3['mpg'], align='center')
#plt.show()



plt.scatter(mtcars[mtcars.am == 0]['qsec'], mtcars[mtcars.am == 0]['hp'], label='automatic')
plt.scatter(mtcars[mtcars.am == 1]['qsec'], mtcars[mtcars.am == 1]['hp'], label='manual')
plt.legend(loc=1)
plt.show()

