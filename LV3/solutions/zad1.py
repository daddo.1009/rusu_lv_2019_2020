# -*- coding: utf-8 -*-
"""
Created on Thu Dec  3 08:16:12 2020

@author: student
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

mtcars = pd.read_csv('../resources/mtcars.csv')
#print(mtcars)


#mtcars_potrosnja = mtcars.sort_values('mpg')
#print(mtcars_potrosnja.head(5)['car'])

#mtcars8cil_potrosnja = mtcars[mtcars.cyl == 8].sort_values('mpg')
#print(mtcars8cil_potrosnja.tail(3)['car'])

#mtcars6cil_potrosnja=mtcars[mtcars.cyl==6]
#srednjapotrosnja_mtcars6cil=mtcars6cil_potrosnja['mpg'].mean()
#print(srednjapotrosnja_mtcars6cil)


#mtcars4cil_2000_2200lbs=mtcars[(mtcars.cyl==4) & (mtcars.wt>=2.000) & (mtcars.wt<=2.200)]
#srednjapotrosnja_4cil_2000_2200=mtcars4cil_2000_2200lbs['mpg'].mean()
#print(srednjapotrosnja_4cil_2000_2200)


#number_of_cars=len(mtcars)
#automatic_cars=len(mtcars[mtcars.am==0])
#print("automatic:", automatic_cars, "  manual:", number_of_cars - automatic_cars)

#automatic_cars_over100hp=len(mtcars[(mtcars.am==0) & (mtcars.hp>100)])
#print( automatic_cars_over100hp)

cars_kg=mtcars[['car', 'wt']]
cars_kg.wt *= 0.453592*1000
print(cars_kg)
