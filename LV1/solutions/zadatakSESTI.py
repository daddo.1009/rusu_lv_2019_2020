import urllib2
import re

i=0             
e_mail = []    
domena = []     
dictionary = {} 
datoteka = urllib2.urlopen("http://www.py4inf.com/code/mbox.txt")

for line in datoteka:               
    line = line.rstrip()           
    if line.startswith('From'):    
        match = re.search(r'[\w\.-]+@[\w\.-]+', line) 
        e_mail.append(match.group(0))   
        i +=1
        
for m in xrange (0, i, 1): 
    domena.append(e_mail[m].split("@")[-1]) 

print ("Ispis svake 250. e-mail adrese:")

for n in xrange (0, i, 250): #ispisujemo svaku 250. adresu na ekran
    print ("e_mail broj %d: ") % (n+1), e_mail[n]

print ("\n")

for x in domena:               
    if x in dictionary:         
        dictionary[x] += 1
    else:                      
        dictionary[x] = 1
 
size=len(dictionary)            

print ("Ispis svakog petog elementa spremljenog u distionary:")
for n in xrange (0, size, 5):   
    print ("Domena: ",dictionary.keys()[n], " >> ponavlja se --", dictionary.values()[n] ,"-- puta")
    

print ("\nVelicina dictionary-a: ", size)
print ("U datoteci mbox-short.txt nalazi se %d e-mail adresa.") % i 
