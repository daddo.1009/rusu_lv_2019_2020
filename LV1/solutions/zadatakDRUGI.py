while True: 
    try:
        number = float(input("Unesite broj: ")) 
        break         
    except ValueError: 
        print ("Niste unjeli broj, pokusajte ponovo!")
        
if number > 1.0 or number < 0.0:
    print("Uneseni broj nije u odgovarajućim infervalima")
elif number >= 0.9 : 
    print ("Uneseni broj", number, "pripada kategoriji A")
elif number >= 0.8 and number < 0.9 :
    print ("Uneseni broj", number, "pripada kategoriji B")
elif number >= 0.7 and number < 0.7 :
    print ("Uneseni broj", number, "pripada kategoriji C")
elif number >= 0.6 and number < 0.7 :
    print ("Uneseni broj", number, "pripada kategoriji D") 
elif number < 0.6 :
    print ("Uneseni broj", number, "pripada kategoriji F")
