import numpy as np
import matplotlib.pyplot as plt
import sklearn.linear_model as linm
import sklearn.neural_network as nn
from sklearn.preprocessing import StandardScaler
import sklearn.metrics as met

def add_noise(y):
    np.random.seed(14)
    varNoise = np.max(y) - np.min(y)
    y_noisy = y + 0.1 * varNoise * np.random.normal(0, 1, len(y))
    return y_noisy


def non_func(n):
    x = np.linspace(1, 10, n)
    y = 1.6345 - 0.6235 * np.cos(0.6067 * x) - 1.3501 * np.sin(0.6067 * x) - 1.1622 * np.cos(
        2 * x * 0.6067) - 0.9443 * np.sin(2 * x * 0.6067)
    y_measured = add_noise(y)
    data = np.concatenate((x, y, y_measured), axis=0)
    data = data.reshape(3, n)
    return data.T

#np.random.seed(242)
train_data = non_func(500)


ytrain = train_data[:, 1]
xtrain = train_data[:, 0]
test_data = non_func(250)

xtrain = xtrain.reshape(-1, 1)
ytrain = ytrain


ytest = test_data[:, 1].reshape(-1, 1)
xtest = test_data[:, 0].reshape(-1, 1)

ann = nn.MLPRegressor(hidden_layer_sizes=(100, 20,),
                      activation="logistic",
                      alpha=0.0001,
                      batch_size=10,
                      max_iter=500)


ann.fit(xtrain, ytrain)
ypred = ann.predict(xtest)

score = ann.score(xtest, ytest)
print(score)