from sklearn.datasets import load_boston
import matplotlib.pyplot as plt
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import PolynomialFeatures
from sklearn.linear_model import LinearRegression
import pandas as pd
import numpy as np
boston = load_boston()
X = boston.data
y = boston.target
print(X.shape)
print(y.shape)

print(boston.feature_names)

df=pd.DataFrame(np.hstack((X,np.expand_dims(y,1))))
print(df.shape)
print(df.info())
print(df.describe())

idx = np.random.permutation(len(X))
idx_train = idx[0:int(np.floor(0.7*len(X)))]
idx_test = idx[int(np.floor(0.7*len(X)))+1:len(X)]

xtrain=X[idx_train]
ytrain=y[idx_train]
xtest=X[idx_test]
ytest=y[idx_test]

degree=6
print("Degree is:",degree)
poly = PolynomialFeatures(degree=degree)
xnew = poly.fit_transform(X)

linearModel = LinearRegression()
linearModel.fit(xtrain,ytrain)

ytrain_p = linearModel.predict(xtrain)
MSE_train=mean_squared_error(ytrain,ytrain_p)
print(MSE_train)

ytest_p = linearModel.predict(xtest)
MSE_test = mean_squared_error(ytest, ytest_p)
print(MSE_test)

print(linearModel.intercept_,linearModel.coef_)
print()
