# -*- coding: utf-8 -*-
"""
Created on Wed Dec  2 01:58:48 2020

@author: Danijel
"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg

img = mpimg.imread('../resources/tiger.png')


imgplot = plt.imshow(img)
plt.show()



gray = lambda rgb : np.dot(rgb[... , :3] , [0.299 , 0.587, 0.114]) 
gray = gray(img)


plt.imshow(gray, cmap = plt.get_cmap(name = 'gray'))
plt.show()
