# -*- coding: utf-8 -*-
"""
Created on Wed Dec  2 01:51:09 2020

@author: Danijel
"""

import numpy as np
import matplotlib.pyplot as plt

np.random.seed() 
die = np.random.randint(low=1, high=7, size=100)
print(die)
plt.hist(die)